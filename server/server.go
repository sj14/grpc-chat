package main

import (
	"io"
	"log"
	server "net"

	pb "gitlab.com/sj14/grpc-chat/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func main() {
	log.Println("starting server on port 8080")
	lis, err := server.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterChatServer(s, &Server{})

	// Register reflection service on gRPC server.
	reflection.Register(s)
	if err := s.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

type Server struct{}

// Broadcast is currently just an echo :D
func (s *Server) Broadcast(stream pb.Chat_BroadcastServer) error {
	for {
		in, err := stream.Recv()
		if err == io.EOF {
			return nil
		}
		if err != nil {
			return err
		}
		log.Printf("received: %v\n", in)
		stream.Send(&pb.Message{Text: in.Text})
	}
}
