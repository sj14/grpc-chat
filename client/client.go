package main

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"os"

	pb "gitlab.com/sj14/grpc-chat/api"

	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("localhost:8080", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("failed dialing: %v", err)
	}
	defer conn.Close()

	ctx := context.Background()
	client := pb.NewChatClient(conn)

	broadcastClient, err := client.Broadcast(ctx)
	if err != nil {
		log.Fatalf("failed creating broadcast client: %v", err)
	}

	go recvMessages(broadcastClient)
	reader := bufio.NewReader(os.Stdin)

	fmt.Println("send something")

	for {
		// Read text from stdin
		text, err := reader.ReadString('\n')
		if err != nil {
			log.Printf("failed reading stdin: %v\n", err)
		}
		// Send text to server
		err = broadcastClient.Send(&pb.Message{Text: text})
		if err != nil {
			log.Printf("failed sending broadcast: %v\n", err)
		}
	}
}

func recvMessages(broadcastClient pb.Chat_BroadcastClient) {
	for {
		msg, err := broadcastClient.Recv()
		if err != nil {
			log.Printf("failed receiving broadcast: %v\n", err)
		}
		fmt.Printf("%v\n", msg.Text)
	}
}
